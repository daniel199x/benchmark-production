
# SpeedTest Go (Fiber) vs Node(Express) in Prod (Deploy with Rancher)

## Prepare:

### NodeJS Code
```js
// NodeJS Version 12
const express = require('express')
const app = express()
 
app.get('/', function (req, res) {
  var str = "";
  for (let i = 0; i < 300; i++){
    str += "A"
  }

  res.json({
      status: 200,
      message: "NE",
      data: "Hello Guy !"
  })
})
 
app.listen(3000)
```

```md
FROM node:12

RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app/

COPY package.json .
RUN npm install

COPY . .

# expose container port
EXPOSE 3000

CMD npm start
```

### Golang Code

```go
// Golang
package main

import (
	"log"

	"github.com/gofiber/fiber/v2"
)

func main() {
	app := fiber.New()

	type SomeStruct struct {
		Status int
		Message  string
		Data string
	}

	app.Get("/", func(c *fiber.Ctx) error {
		str := ""
		for i:= 0; i < 300; i++ {
			str += "A"
		}
		data := SomeStruct{
			Status: 200,
			Message:  "GF",
			Data: "Hello Guy !",
		}
		
		return c.JSON(data)
	})

	log.Fatal(app.Listen(":3000"))
}
```

```md
FROM golang:latest
WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY . .
RUN go build -o main .
EXPOSE 3000

# Command to run the executable
CMD ["./main"]
```


## Result

- Benchmark with autocanon with low concurrent 
  - Avg : 57 ms, 13k request (NodeJS)
  - Avg : 25 ms, 29k request (Golang)
![image info](https://iili.io/2am0Yv.png)

- Benchmark with hey with low concurrent
  - Avg : 97 ms, 1128 request (NodeJS)
  - Avg : 96 ms, 1454 request (Golang)
![image info](https://iili.io/2am1vR.png)

- ==> In most cases Golang is bester, But when increate concurrent
  - Avg : 63 ms, 1444 request (NodeJS) ??? :D
  - Avg : 60 ms, 1042 request (Golang)  ??? :D
![image info](https://iili.io/2amVjI.png)


- CPU, RAM used
    - Nodejs (expressjs) : Avg: 54.5 Mb
    ![image info](https://iili.io/2amMTN.png)

    - Golang (fiber) : Avg : 6.7 Mb
    ![image info](https://iili.io/2amEpp.png)





